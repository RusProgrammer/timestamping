// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/structs/EnumerableMap.sol";

contract TimeStamping {
    using EnumerableMap for EnumerableMap.Bytes32ToUintMap;

    EnumerableMap.Bytes32ToUintMap private _history;

    function createStamp(bytes32 hash_) external {
        require(_history.set(hash_, block.timestamp), "Hash collision");
    }

    function getHashStamp(bytes32 hash_) external view returns (uint256) {
        bool succes_;
        uint256 result_;

        (succes_, result_) = _history.tryGet(hash_);

        require(succes_, "Hash is not existing");
        return result_;
    }
}
